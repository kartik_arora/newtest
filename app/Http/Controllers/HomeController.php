<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Cart;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $get_shelves = DB::table('shelves')->take(10)->get();
        foreach ($get_shelves as $shelves) {
            $data[$shelves->label] =  DB::table('products')->select('products.id as product_id', 'price', 'label', 'product_type_id', 'shelve_id', 'name')->join('product_types', 'products.product_type_id', '=', 'product_types.id')->where('shelve_id','=',$shelves->id)->take(25)->get();
        }
        return view('home')->with('products', $data)->with('status', $request->input('message'));
    }

    public function addToCart(Request $request) {

        $product_details = DB::table('products')->where('id','=',$request->input('product_id'))->get();
        if(Cart::add($request->input('product_id'), $product_details[0]->name, 1, $product_details[0]->price)) {
            return json_encode(array('status' => 1, 'message' => 'success' ));
        } else {
            return json_encode(array('status' => 0, 'message' => 'Not added to cart' ));
        }


    }


    public function getCart() {

        $cart = Cart::content();
        return view('cart')->with('cart_data', $cart);

    }

     public function checkout() {

        $cart = Cart::content();
        $cart_total = Cart::subtotal();

        $user_wallet = Auth::user()->wallet;
        $cart_total = floor(str_replace(',', '', $cart_total));

        if($user_wallet <= $cart_total){

            return redirect('/home?message=Your balance is not sufficient');

        }

        foreach ($cart as $carts) {
            $get_shelve_id = DB::table('products')->where('id', '=', $carts->id)->first();
            $add_query =  DB::table('user_products')->insert(
                            ['user_id' => Auth::user()->id , 'product_id' => $carts->id, 'product_name'=>$carts->name, 'price' => $carts->price, 'shelve_id' => $get_shelve_id->shelve_id]
                        );
            $query = DB::table('products')->where('id', '=', $carts->id)->delete();
        }
        $cart = Cart::destroy();


        $update_user_wallet = DB::table('users')->where('id', Auth::user()->id)->update(['wallet' => ($user_wallet - $cart_total)]);

        $this->fillApples(); //refill shleves with apples

        return redirect('/home?message=You have successfully purchased the product');

    }


    private function fillApples() {

        $query = DB::table('products')->select(DB::raw('count(*) as number_products, shelve_id'))->groupBy('shelve_id')->get();
            foreach($query as $data) {
                if($data->number_products <= 25) {
                    for($i=0;$i<(25-$data->shelve_id);$i++){
                        DB::table('products')->insert(
                            ['name' => 'Apple ' . $i, 'price' => rand(400,900), 'product_type_id'=>1, 'shelve_id' => $data->shelve_id]
                        );
                    }

                }
            }

    }
}
