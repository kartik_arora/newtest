-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 20, 2017 at 04:09 AM
-- Server version: 5.6.33-0ubuntu0.14.04.1
-- PHP Version: 5.6.21-1+donate.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `newtest`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `price` float NOT NULL,
  `product_type_id` int(11) NOT NULL,
  `shelve_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=337 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `product_type_id`, `shelve_id`) VALUES
(2, 'Apple 2', 500, 1, 9),
(3, 'Apple 3', 120, 1, 2),
(4, 'Apple New', 521, 1, 2),
(5, 'Apple 5', 461, 1, 1),
(8, 'Apple 8', 767, 1, 10),
(11, 'Apple 11', 673, 1, 2),
(12, 'Apple 12', 50, 1, 2),
(13, 'Apple 13', 907, 1, 2),
(14, 'Apple 14', 142, 1, 2),
(15, 'Apple 15', 999, 1, 2),
(16, 'Apple 16', 192, 1, 2),
(17, 'Apple 17', 979, 1, 2),
(18, 'Apple 18', 139, 1, 2),
(19, 'Apple 19', 743, 1, 2),
(20, 'Apple 20', 954, 1, 2),
(21, 'Apple 21', 635, 1, 2),
(22, 'Apple 22', 161, 1, 3),
(23, 'Apple 23', 455, 1, 3),
(24, 'Apple 24', 71, 1, 3),
(25, 'Apple 25', 479, 1, 3),
(26, 'Apple 26', 554, 1, 3),
(27, 'Apple 27', 363, 1, 3),
(28, 'Apple 28', 856, 1, 3),
(29, 'Apple 29', 386, 1, 3),
(30, 'Apple 30', 139, 1, 3),
(31, 'Apple 31', 913, 1, 3),
(32, 'Apple 32', 838, 1, 3),
(33, 'Apple 33', 672, 1, 4),
(34, 'Apple 34', 195, 1, 4),
(35, 'Apple 35', 138, 1, 4),
(36, 'Apple 36', 173, 1, 4),
(37, 'Apple 37', 882, 1, 4),
(38, 'Apple 38', 981, 1, 4),
(39, 'Apple 39', 714, 1, 4),
(40, 'Apple 40', 785, 1, 4),
(41, 'Apple 41', 602, 1, 4),
(42, 'Apple 42', 932, 1, 4),
(43, 'Apple 43', 576, 1, 4),
(44, 'Apple 44', 196, 1, 5),
(45, 'Apple 45', 479, 1, 5),
(46, 'Apple 46', 173, 1, 5),
(47, 'Apple 47', 464, 1, 5),
(48, 'Apple 48', 766, 1, 5),
(49, 'Apple 49', 259, 1, 5),
(50, 'Apple 50', 751, 1, 5),
(51, 'Apple 51', 776, 1, 5),
(52, 'Apple 52', 170, 1, 5),
(53, 'Apple 53', 125, 1, 5),
(54, 'Apple 54', 725, 1, 5),
(55, 'Apple 55', 631, 1, 6),
(56, 'Apple 56', 521, 1, 6),
(57, 'Apple 57', 153, 1, 6),
(58, 'Apple 58', 998, 1, 6),
(59, 'Apple 59', 384, 1, 6),
(60, 'Apple 60', 131, 1, 6),
(61, 'Apple 61', 979, 1, 6),
(62, 'Apple 62', 287, 1, 6),
(63, 'Apple 63', 610, 1, 6),
(64, 'Apple 64', 826, 1, 6),
(65, 'Apple 65', 92, 1, 6),
(66, 'Apple 66', 467, 1, 7),
(67, 'Apple 67', 556, 1, 7),
(68, 'Apple 68', 529, 1, 7),
(69, 'Apple 69', 160, 1, 7),
(70, 'Apple 70', 333, 1, 7),
(71, 'Apple 71', 552, 1, 7),
(72, 'Apple 72', 150, 1, 7),
(73, 'Apple 73', 240, 1, 7),
(74, 'Apple 74', 994, 1, 7),
(75, 'Apple 75', 844, 1, 7),
(76, 'Apple 76', 837, 1, 7),
(77, 'Apple 77', 186, 1, 8),
(78, 'Apple 78', 272, 1, 8),
(79, 'Apple 79', 555, 1, 8),
(80, 'Apple 80', 526, 1, 8),
(87, 'Apple 6', 752, 1, 1),
(88, 'Apple 7', 820, 1, 1),
(89, 'Apple 8', 837, 1, 1),
(90, 'Apple 0', 434, 1, 8),
(91, 'Apple 1', 403, 1, 8),
(93, 'Apple 1', 749, 1, 8),
(94, 'Apple 0', 723, 1, 1),
(95, 'Apple 1', 835, 1, 1),
(97, 'Apple 3', 524, 1, 1),
(98, 'Apple 4', 501, 1, 1),
(99, 'Apple 5', 584, 1, 1),
(100, 'Apple 6', 501, 1, 1),
(101, 'Apple 7', 781, 1, 1),
(102, 'Apple 8', 838, 1, 1),
(103, 'Apple 9', 417, 1, 1),
(104, 'Apple 10', 664, 1, 1),
(105, 'Apple 11', 820, 1, 1),
(106, 'Apple 12', 733, 1, 1),
(107, 'Apple 13', 582, 1, 1),
(108, 'Apple 14', 624, 1, 1),
(109, 'Apple 15', 416, 1, 1),
(110, 'Apple 16', 495, 1, 1),
(111, 'Apple 17', 429, 1, 1),
(112, 'Apple 18', 887, 1, 1),
(113, 'Apple 19', 576, 1, 1),
(114, 'Apple 20', 513, 1, 1),
(115, 'Apple 21', 564, 1, 1),
(116, 'Apple 22', 878, 1, 1),
(117, 'Apple 23', 587, 1, 1),
(118, 'Apple 0', 415, 1, 2),
(119, 'Apple 1', 798, 1, 2),
(120, 'Apple 2', 524, 1, 2),
(121, 'Apple 3', 450, 1, 2),
(122, 'Apple 4', 802, 1, 2),
(123, 'Apple 5', 628, 1, 2),
(124, 'Apple 6', 799, 1, 2),
(125, 'Apple 7', 624, 1, 2),
(126, 'Apple 8', 563, 1, 2),
(127, 'Apple 9', 617, 1, 2),
(128, 'Apple 10', 749, 1, 2),
(129, 'Apple 11', 665, 1, 2),
(130, 'Apple 12', 802, 1, 2),
(131, 'Apple 13', 851, 1, 2),
(132, 'Apple 14', 545, 1, 2),
(133, 'Apple 15', 739, 1, 2),
(134, 'Apple 16', 868, 1, 2),
(135, 'Apple 17', 810, 1, 2),
(136, 'Apple 18', 659, 1, 2),
(137, 'Apple 19', 701, 1, 2),
(138, 'Apple 20', 491, 1, 2),
(139, 'Apple 21', 884, 1, 2),
(140, 'Apple 22', 717, 1, 2),
(141, 'Apple 0', 586, 1, 3),
(142, 'Apple 1', 412, 1, 3),
(143, 'Apple 2', 704, 1, 3),
(144, 'Apple 3', 763, 1, 3),
(145, 'Apple 4', 525, 1, 3),
(146, 'Apple 5', 868, 1, 3),
(147, 'Apple 6', 741, 1, 3),
(148, 'Apple 7', 713, 1, 3),
(149, 'Apple 8', 884, 1, 3),
(150, 'Apple 9', 639, 1, 3),
(151, 'Apple 10', 838, 1, 3),
(152, 'Apple 11', 433, 1, 3),
(153, 'Apple 12', 540, 1, 3),
(154, 'Apple 13', 565, 1, 3),
(155, 'Apple 14', 833, 1, 3),
(156, 'Apple 15', 765, 1, 3),
(157, 'Apple 16', 729, 1, 3),
(158, 'Apple 17', 550, 1, 3),
(159, 'Apple 18', 613, 1, 3),
(160, 'Apple 19', 493, 1, 3),
(161, 'Apple 20', 451, 1, 3),
(162, 'Apple 21', 564, 1, 3),
(163, 'Apple 0', 639, 1, 4),
(164, 'Apple 1', 791, 1, 4),
(165, 'Apple 2', 531, 1, 4),
(166, 'Apple 3', 548, 1, 4),
(167, 'Apple 4', 549, 1, 4),
(168, 'Apple 5', 833, 1, 4),
(169, 'Apple 6', 639, 1, 4),
(170, 'Apple 7', 532, 1, 4),
(171, 'Apple 8', 649, 1, 4),
(172, 'Apple 9', 826, 1, 4),
(173, 'Apple 10', 545, 1, 4),
(174, 'Apple 11', 453, 1, 4),
(175, 'Apple 12', 689, 1, 4),
(176, 'Apple 13', 671, 1, 4),
(177, 'Apple 14', 420, 1, 4),
(178, 'Apple 15', 529, 1, 4),
(179, 'Apple 16', 483, 1, 4),
(180, 'Apple 17', 403, 1, 4),
(181, 'Apple 18', 769, 1, 4),
(182, 'Apple 19', 421, 1, 4),
(183, 'Apple 20', 437, 1, 4),
(184, 'Apple 0', 408, 1, 5),
(185, 'Apple 1', 586, 1, 5),
(186, 'Apple 2', 870, 1, 5),
(187, 'Apple 3', 774, 1, 5),
(188, 'Apple 4', 415, 1, 5),
(189, 'Apple 5', 520, 1, 5),
(190, 'Apple 6', 486, 1, 5),
(191, 'Apple 7', 509, 1, 5),
(192, 'Apple 8', 571, 1, 5),
(193, 'Apple 9', 650, 1, 5),
(194, 'Apple 10', 748, 1, 5),
(195, 'Apple 11', 461, 1, 5),
(196, 'Apple 12', 782, 1, 5),
(197, 'Apple 13', 896, 1, 5),
(198, 'Apple 14', 610, 1, 5),
(199, 'Apple 15', 714, 1, 5),
(200, 'Apple 16', 635, 1, 5),
(201, 'Apple 17', 743, 1, 5),
(202, 'Apple 18', 463, 1, 5),
(203, 'Apple 19', 561, 1, 5),
(204, 'Apple 0', 889, 1, 6),
(205, 'Apple 1', 516, 1, 6),
(206, 'Apple 2', 851, 1, 6),
(207, 'Apple 3', 659, 1, 6),
(208, 'Apple 4', 537, 1, 6),
(209, 'Apple 5', 480, 1, 6),
(210, 'Apple 6', 743, 1, 6),
(211, 'Apple 7', 541, 1, 6),
(212, 'Apple 8', 849, 1, 6),
(213, 'Apple 9', 764, 1, 6),
(214, 'Apple 10', 578, 1, 6),
(215, 'Apple 11', 857, 1, 6),
(216, 'Apple 12', 450, 1, 6),
(217, 'Apple 13', 548, 1, 6),
(218, 'Apple 14', 730, 1, 6),
(219, 'Apple 15', 465, 1, 6),
(220, 'Apple 16', 668, 1, 6),
(221, 'Apple 17', 817, 1, 6),
(222, 'Apple 18', 574, 1, 6),
(223, 'Apple 0', 840, 1, 7),
(224, 'Apple 1', 567, 1, 7),
(225, 'Apple 2', 422, 1, 7),
(226, 'Apple 3', 400, 1, 7),
(227, 'Apple 4', 449, 1, 7),
(228, 'Apple 5', 417, 1, 7),
(229, 'Apple 6', 611, 1, 7),
(230, 'Apple 7', 763, 1, 7),
(231, 'Apple 8', 653, 1, 7),
(232, 'Apple 9', 454, 1, 7),
(233, 'Apple 10', 827, 1, 7),
(234, 'Apple 11', 815, 1, 7),
(235, 'Apple 12', 442, 1, 7),
(236, 'Apple 13', 443, 1, 7),
(237, 'Apple 14', 765, 1, 7),
(238, 'Apple 15', 702, 1, 7),
(239, 'Apple 16', 580, 1, 7),
(240, 'Apple 17', 845, 1, 7),
(241, 'Apple 0', 544, 1, 8),
(242, 'Apple 1', 722, 1, 8),
(243, 'Apple 2', 793, 1, 8),
(244, 'Apple 3', 408, 1, 8),
(245, 'Apple 4', 900, 1, 8),
(246, 'Apple 5', 750, 1, 8),
(247, 'Apple 6', 459, 1, 8),
(248, 'Apple 7', 548, 1, 8),
(249, 'Apple 8', 579, 1, 8),
(250, 'Apple 9', 524, 1, 8),
(251, 'Apple 10', 817, 1, 8),
(252, 'Apple 11', 496, 1, 8),
(253, 'Apple 12', 699, 1, 8),
(254, 'Apple 13', 756, 1, 8),
(255, 'Apple 14', 663, 1, 8),
(256, 'Apple 15', 722, 1, 8),
(257, 'Apple 16', 756, 1, 8),
(258, 'Apple 0', 712, 1, 9),
(259, 'Apple 1', 740, 1, 9),
(260, 'Apple 2', 467, 1, 9),
(261, 'Apple 3', 575, 1, 9),
(262, 'Apple 4', 492, 1, 9),
(263, 'Apple 5', 521, 1, 9),
(264, 'Apple 6', 502, 1, 9),
(265, 'Apple 7', 407, 1, 9),
(266, 'Apple 8', 564, 1, 9),
(267, 'Apple 9', 545, 1, 9),
(268, 'Apple 10', 772, 1, 9),
(269, 'Apple 11', 867, 1, 9),
(270, 'Apple 12', 726, 1, 9),
(271, 'Apple 13', 717, 1, 9),
(272, 'Apple 14', 511, 1, 9),
(273, 'Apple 15', 547, 1, 9),
(274, 'Apple 0', 609, 1, 10),
(275, 'Apple 1', 519, 1, 10),
(276, 'Apple 2', 547, 1, 10),
(277, 'Apple 3', 458, 1, 10),
(278, 'Apple 4', 578, 1, 10),
(279, 'Apple 5', 695, 1, 10),
(280, 'Apple 6', 638, 1, 10),
(281, 'Apple 7', 703, 1, 10),
(282, 'Apple 8', 611, 1, 10),
(283, 'Apple 9', 734, 1, 10),
(284, 'Apple 10', 502, 1, 10),
(285, 'Apple 11', 467, 1, 10),
(286, 'Apple 12', 496, 1, 10),
(287, 'Apple 13', 825, 1, 10),
(288, 'Apple 14', 824, 1, 10),
(289, 'Apple 0', 809, 1, 8),
(290, 'Apple 1', 664, 1, 8),
(291, 'Apple 2', 891, 1, 8),
(292, 'Apple 3', 483, 1, 8),
(293, 'Apple 4', 756, 1, 8),
(294, 'Apple 5', 512, 1, 8),
(295, 'Apple 6', 585, 1, 8),
(296, 'Apple 7', 763, 1, 8),
(297, 'Apple 8', 677, 1, 8),
(298, 'Apple 9', 731, 1, 8),
(299, 'Apple 10', 635, 1, 8),
(300, 'Apple 11', 643, 1, 8),
(301, 'Apple 12', 556, 1, 8),
(302, 'Apple 13', 451, 1, 8),
(303, 'Apple 14', 755, 1, 8),
(304, 'Apple 15', 703, 1, 8),
(305, 'Apple 16', 661, 1, 8),
(306, 'Apple 0', 875, 1, 9),
(307, 'Apple 1', 850, 1, 9),
(308, 'Apple 2', 719, 1, 9),
(309, 'Apple 3', 553, 1, 9),
(310, 'Apple 4', 645, 1, 9),
(311, 'Apple 5', 456, 1, 9),
(312, 'Apple 6', 856, 1, 9),
(313, 'Apple 7', 856, 1, 9),
(314, 'Apple 8', 791, 1, 9),
(315, 'Apple 9', 458, 1, 9),
(316, 'Apple 10', 422, 1, 9),
(317, 'Apple 11', 887, 1, 9),
(318, 'Apple 12', 883, 1, 9),
(319, 'Apple 13', 847, 1, 9),
(320, 'Apple 14', 796, 1, 9),
(321, 'Apple 15', 646, 1, 9),
(322, 'Apple 0', 837, 1, 10),
(323, 'Apple 1', 879, 1, 10),
(324, 'Apple 2', 502, 1, 10),
(325, 'Apple 3', 449, 1, 10),
(326, 'Apple 4', 564, 1, 10),
(327, 'Apple 5', 866, 1, 10),
(328, 'Apple 6', 726, 1, 10),
(329, 'Apple 7', 896, 1, 10),
(330, 'Apple 8', 601, 1, 10),
(331, 'Apple 9', 469, 1, 10),
(332, 'Apple 10', 551, 1, 10),
(333, 'Apple 11', 652, 1, 10),
(334, 'Apple 12', 824, 1, 10),
(335, 'Apple 13', 855, 1, 10),
(336, 'Apple 14', 413, 1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

CREATE TABLE IF NOT EXISTS `product_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(190) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `product_types`
--

INSERT INTO `product_types` (`id`, `label`, `description`) VALUES
(1, 'Apple', ''),
(2, 'Orange', '');

-- --------------------------------------------------------

--
-- Table structure for table `shelves`
--

CREATE TABLE IF NOT EXISTS `shelves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` text NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `shelves`
--

INSERT INTO `shelves` (`id`, `label`, `description`) VALUES
(1, 'Shelve A', ''),
(2, 'Shelve B', ''),
(3, 'Shelve 2', 'desc'),
(4, 'Shelve 3', 'desc'),
(5, 'Shelve 4', 'desc'),
(6, 'Shelve 5', 'desc'),
(7, 'Shelve 6', 'desc'),
(8, 'Shelve 7', 'desc'),
(9, 'Shelve 8', 'desc'),
(10, 'Shelve 9', 'desc'),
(11, 'Shelve 10', 'desc'),
(12, 'Shelve 11', 'desc'),
(13, 'Shelve 12', 'desc');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wallet` float NOT NULL DEFAULT '1000',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `wallet`, `created_at`, `updated_at`) VALUES
(1, 'kartik', 'me@kartikarora.in', '$2y$10$JEEg/gsW6JGvqeP/YzuI5uTQeYpUKpUZSHSqOzUB8XfIKUbTV0taG', NULL, 1822, '2017-04-19 09:29:39', '2017-04-19 09:29:39');

-- --------------------------------------------------------

--
-- Table structure for table `user_products`
--

CREATE TABLE IF NOT EXISTS `user_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` text NOT NULL,
  `price` float NOT NULL,
  `shelve_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `user_products`
--

INSERT INTO `user_products` (`id`, `user_id`, `product_id`, `product_name`, `price`, `shelve_id`) VALUES
(1, 1, 9, 'Apple 9', 870, 1),
(2, 1, 10, 'Apple 10', 883, 1),
(3, 1, 81, 'Apple 0', 871, 1),
(4, 1, 82, 'Apple 1', 481, 1),
(5, 1, 96, 'Apple 2', 718, 1),
(6, 1, 84, 'Apple 3', 577, 1),
(7, 1, 83, 'Apple 2', 484, 1),
(8, 1, 86, 'Apple 5', 474, 1),
(9, 1, 85, 'Apple 4', 702, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
