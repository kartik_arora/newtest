@extends('layouts.app')

@section('content')
<div class="container">

<div class="row">
<div class="col-md-12">
<div id="status"></div>
</div>
</div>

    <div class="row">
        <table class="table table-bordered table-responsive">
          <thead>
            <th>Product ID</th>
            <th>Product Name</th>
            <th>Price</th>
            <th>Quantity</th>
          </thead>

          <tbody>
              @foreach($cart_data as $cart)
                <tr>
                  <td>{{$cart->id}}</td>
                  <td>{{$cart->name}}</td>
                  <td>{{$cart->price}}</td>
                  <td>{{$cart->qty}}</td>
                </tr>
                @endforeach
          </tbody>
        </table>
    </div>

    <div class="row">
    <div class="col-md-12">
    <a href="/checkout" class="col-md-3 btn btn-primary" > Buy </a>
    </div>

    </div>
</div>
@endsection
