@extends('layouts.app')

@section('content')
<div class="container">

<div class="row">
<div class="col-md-12">
<div id="status">
    @if(isset($status) && !empty($status))

       <span class="col-md-12 alert alert-warning"> {{$status}} </span>
 
    @endif


</div>
</div>
</div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
                @foreach ($products as  $key=>$products)
            <div class="panel panel-default">
                <div class="panel-heading">{{$key}}</div>

                <div class="panel-body">
                <div class="row">
                @foreach ($products as $product)

                    <div class="col-sm-6 col-md-3">
                                        <div class="thumbnail">
                                          <img src="http://placehold.it/500x300" alt="...">
                                          <div class="caption text-center">
                                            <h3>{{$product->name}}</h3>                                        
                                          </div><!--end caption-->
                                          <div class="caption text-center" style="padding:2px;"> Price  : <strong>INR {{$product->price}} </strong> </div>
                                          <br>
                                          <a style="margin-left:15%;" data-productkey='{{$product->product_id}}' class="btn btn-warning add-to-cart" role="button"> Add to cart <i class="fa fa-angle-right"></i></a>
                                        </div><!--end thumbnail-->
                                      </div>
                @endforeach



                </div>


                </div>
            </div>
                            @endforeach

        </div>
    </div>
</div>
@endsection
